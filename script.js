const button = document.getElementById("btn-click");
const sectionWithParagraphs = document.getElementById("content");
function createParagraph() {
  let p = document.createElement("p");
  p.textContent = "New Paragraph";
  sectionWithParagraphs.append(p);
}
button.addEventListener("click", createParagraph);

const buttonInput = document.getElementById("btn-input-create");
function createInput() {
  let newInput = document.createElement("input");
  newInput.setAttribute("name", "newInput");
  newInput.setAttribute("placeholder", "text");
  newInput.setAttribute("type", "text");
  sectionWithParagraphs.append(newInput);
  newInput.style.display = "block";
}
buttonInput.addEventListener("click", createInput);
